class CreateRenderTurboStreamOptions < ActiveRecord::Migration[7.0]
  def change
    create_table :render_turbo_stream_options do |t|
      t.string :title

      t.timestamps
    end
  end
end
