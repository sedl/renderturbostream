module RenderTurboStream
  module Test
    module System
      module Helpers

        def target_id(relative_view_path, object)
          libs = RenderTurboStream::Libs
          libs.target_id(relative_view_path, object)
        end

        def target_id_css(relative_view_path, object)
          "##{target_id(relative_view_path, object)}"
        end

      end
    end
  end
end
