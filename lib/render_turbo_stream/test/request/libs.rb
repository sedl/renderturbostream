module RenderTurboStream
  module Test
    module Request
      class Libs

        def initialize(response)
          @response = response
        end

        def self.set_test_responses(response, test_responses)
          if Rails.env.test?
            h = response.headers.to_h
            i = 1
            test_responses.each do |test_response|
              loop do
                k = "turbo-response-test-#{i}"
                unless h.keys.include?(k)
                  response.headers[k] = test_response.to_json
                  break
                end
                i += 1
              end
            end
          end
        end

        def all_turbo_responses

          res = []
          @response.headers.each do |k, v|
            next unless k.match(/^turbo-response-test-[\d]+$/)
            h = JSON.parse(v)
            res.push(h)
          end
          res
        end

        # checks only for rendered partials, not for actions

        def select_responses(channel, target, action, prohibit_multiple_to_same_target: [:replace], &block)
          all = all_turbo_responses
          res = { log: [], responses: [] }
          raise 'Missing argument: target' unless target.present?

          if !target.present?
            res[:log].push("missing argument: target")
          elsif prohibit_multiple_to_same_target.present?
            has_prohibited_action = false
            c = all.select do |e|
              if prohibit_multiple_to_same_target.include?(e['action']&.to_sym)
                has_prohibited_action = true
              end
              e['target'] == target
            end.length
            if c >= 2 && has_prohibited_action
              res[:log].push("Target «#{target}» is targeted #{c} #{'time'.pluralize(c)} which is prohibited for the actions #{prohibit_multiple_to_same_target.join(', ')}")
              return res
            end
          end

          target_matched = false
          permitted_types = if channel == false
                             ['stream-partial', 'stream-template']
                           else
                             ['stream-partial', 'stream-template', 'channel-partial', 'channel-template']
                           end

          responses = all.select do |a|

            if a['target'] == target
              target_matched = true
            end

            if a['target'] != target
              false
            elsif !permitted_types.include?(a['type'])
              res[:log].push("Type «#{a['type']}» does not match the types you are looking for: #{permitted_types}")
              false
            elsif channel.present? && a['channel'] != channel.to_s
              res[:log].push("Target «#{target}»: Channel #{a['channel']} not matching with given channel «#{channel}»")
              false
            elsif action.present? && a['action'] != action.to_s
              res[:log].push("Target «#{target}»: Action «#{a['action']}» not matching with given action «#{action}»")
              false
            elsif block_given?
                nok = Nokogiri::HTML(a['html_response'])
                if yield(nok).present?
                  true
                else
                  res[:log].push("Target «#{target}»: Given block not matching in => «#{nok.inner_html}»")
                  false
                end
            else
              true
            end
          end

          unless target_matched
            res[:log].push("Target «#{target}» not found in targets: #{turbo_targets}")
          end

          res[:responses] = responses
          res
        end

        # checks only for commands, not for rendered partials
        def select_actions(channel, action, &block)
          all = all_turbo_responses
          res = { log: [], responses: [] }
          possible_types = ['stream-command', 'channel-command']
          raise 'Missing argument: action' unless action.present?

          action_found = false

          responses = all.select do |a|
            if a['action'] == action.to_s
              action_found = true
            end

            if a['action'] != action.to_s
              false
            elsif !possible_types.include?(a['type'])
              res[:log].push("Action «#{action}» is type «#{a['type']}» (accepted types are: #{possible_types.join(', ')})")
              false
            elsif channel.present? && a['channel'] != channel.to_s
              res[:log].push("Channel «#{channel}» not found within action «#{action}»")
              false
            elsif block_given?
                args = a['array'][1..-1]
                if yield(args)
                  true
                else
                  res[:log].push("Given block not matching Arguments => «#{args}»")
                  false
                end
            else
              true
            end
          end

          unless action_found
            res[:log].push("Action «#{action}» not found under responded actions: #{turbo_actions}")
          end

          res[:responses] = responses
          res
        end

        def turbo_targets
          responses = all_turbo_responses
          targets = []
          responses.each do |r|
            if r['target'].is_a?(Array)
              r['target'].each do |t|
                targets.push(t) unless targets.include?(t)
              end
            else
              targets.push(r['target']) unless targets.include?(r['target'])
            end
          end

          targets
        end

        def turbo_actions
          responses = all_turbo_responses
          actions = []
          responses.each do |r|
            actions.push(r['action']) unless actions.include?(r['action'])
          end

          actions
        end

        def targets_counts
          responses = all_turbo_responses
          targets = {}
          responses.each do |r|
            targets[r['target']] ||= 0
            targets[r['target']] += 1
          end

          targets
        end

        def self.assert_error_message(expected, received, log)
          [
            "Expected #{expected} #{'response'.pluralize(expected)} but found #{received}.",
            (log.present? ? "Messages => «#{log.join(', ')}»" : nil)
          ].compact.join(' ')
        end

        def self.first_arg_is_html_id(method)
          config = Rails.configuration.x.render_turbo_stream.first_argument_is_html_id
          default = [
            :graft,
            :morph,
            :inner_html,
            :insert_adjacent_text,
            :outer_html,
            :text_content,
            :add_css_class,
            :remove_attribute,
            :remove_css_class,
            :set_attribute,
            :set_dataset_attribute,
            :set_property,
            :set_style,
            :set_styles,
            :set_value,
            :dispatch_event,
            :reset_form,
            :clear_storage,
            :scroll_into_view,
            :set_focus,
            :turbo_frame_reload,
            :turbo_frame_set_src,
            :replace,
            :append,
            :prepend
          ]
          (config.present? ? config : default).map { |m| m.to_sym }.include?(method.to_sym)
        end

      end
    end
  end
end
