module RenderTurboStream
  module Test
    module Request
      module Helpers

        # Assert that each of given target_ids is targeted exactly once

        def assert_once_targeted(*target_ids)
          responses = RenderTurboStream::Test::Request::Libs.new(response).all_turbo_responses
          id_counts = {}
          responses.each do |r|
            if r['target'].is_a?(Array)
              r['target'].each do |t|
                id = (t[0] == '#' ? t[1..-1] : t)
                id_counts[t.to_s] ||= 0
                id_counts[t.to_s] += 1
              end
            else
              id = (r['target'][0] == '#' ? r['target'][1..-1] : r['target'])
              id_counts[id] ||= 0
              id_counts[id] += 1
            end
          end

          assert(id_counts.keys.length == target_ids.length, "You checked for #{target_ids.length} but #{id_counts.keys.length} targeted: #{id_counts.keys.join(', ')}")
          target_ids.each do |id|
            assert(id_counts.key?(id), "The id #{id} is not within the targeted ids: #{id_counts.keys.join(', ')}")
            expect(id_counts[id]).to eq(1)
            assert(id_counts[id] == 1, "The id #{id} is targeted #{id_counts[id]} #{'time'.pluralize(id_counts[id])}")
          end
        end

        # Helper for the developer for writing tests: Array with all attributes of all turbo-stream and turbo-channel actions that runned on the last request

        def all_turbo_responses
          RenderTurboStream::Test::Request::Libs.new(response).all_turbo_responses
        end

        # Returns Array of all target_ids that arre affected at least once on the last response
        def turbo_targets
          RenderTurboStream::Test::Request::Libs.new(response).turbo_targets
        end

        # Returns the path sent to turbo_stream.redirect_to.
        def assert_turbo_redirect_to(path)
          resps = RenderTurboStream::Test::Request::Libs.new(response).all_turbo_responses
          url = nil
          resps.each do |r|
            if r['type'] == 'stream-command'
              if r['array'].first == 'redirect_to'
                if url
                  url = 'ERROR: REDIRECT CALLED MORE THAN ONCE'
                else
                  url = r['array'].second
                end
              end
            end
          end
          assert url == path, "Expected turbo_redirect_to «#{path}» but got: «#{url}»"
        end

        # assert one or more specific actions to a target_id
        def assert_stream_response(target_id, action: nil, count: 1, &block)

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_responses( false, RenderTurboStream::Libs.target_id_to_target(target_id), action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        def assert_stream_action(action, target_id: nil, count: 1, &block)

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_actions( false, action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        def target_id(relative_view_path, object)
          libs = RenderTurboStream::Libs
          libs.target_id(relative_view_path, object)
        end

        def target_id_css(relative_view_path, object)
          "##{target_id(relative_view_path, object)}"
        end

      end
    end
  end
end
