module RenderTurboStream
  module ChannelViewHelpers

    def channel_from_me
      if current_user
        turbo_stream_from "authenticated_user_#{current_user.id}"
      else
        Rails.logger.debug("  • SKIP channel_from_me because not authenticated")
        nil
      end
    end

    def channel_from_authenticated_group(group)
      if current_user
        turbo_stream_from "authenticated_group_#{group}"
      else
        Rails.logger.debug("  • SKIP channel_from_authenticated_group because not authenticated")
        nil
      end
    end

    def channel_from_all
      turbo_stream_from "all"
    end

    def channel_from_group(group)
      if group.present?
        turbo_stream_from group
      else
        Rails.logger.debug("  • SKIP channel_from_group because no group given")
        nil
      end
    end

    def fetch_instance_variables(locals)
      locals.each do |k,v|
        if k[0..38] == 'render_turbo_stream_instance_variable_@'
          eval "#{k[38..-1]} = v"
        end
      end
    end

  end
end