module RenderTurboStream
  module ChannelControllerHelpers

    def render_to_all(target_id = nil, action = :replace, partial: nil, template: nil, locals: nil)
      # evaluate_instance_variables
      render_to_channel(
        'all',
        target_id,
        action,
        partial: partial,
        template: template,
        locals: locals
      )
    end

    def render_to_me(target_id = nil, action = :replace, partial: nil, locals: nil)
      begin
        u_id = helpers.current_user&.id
        unless u_id.present?
          Rails.logger.debug('  • SKIP RenderTurboStream.render_to_me because current_user is nil')
          return
        end
      rescue
        Rails.logger.debug('  • ERROR RenderTurboStream.render_to_me because current_user is not available')
        return
      end

      render_to_channel(
        "authenticated_user_#{helpers.current_user.id}",
        target_id,
        action,
        partial: partial,
        locals: locals
      )
    end

    def render_to_authenticated_group(group, target_id = nil, action = :replace, partial: nil, locals: nil)
      begin
        u_id = helpers.current_user&.id
        unless u_id.present?
          Rails.logger.debug('  • SKIP RenderTurboStream.render_to_authenticated_group because current_user is nil')
          return
        end
      rescue
        Rails.logger.debug('  • ERROR RenderTurboStream.render_to_authenticated_group because current_user method is not available')
        return
      end

      render_to_channel(
        "authenticated_group_#{group}",
        target_id,
        action,
        partial: partial,
        locals: locals
      )
    end

    def render_to_channel(channel, target_id = nil, action = :replace, partial: nil, template: nil, locals: nil)

      raise 'Arguments partial and template cannot both be specified' if partial && template

      disable_default = false
      if partial.present?
        _partial = RenderTurboStream::Libs.partial_path(controller_path, partial)
      elsif template.present?
        _template = RenderTurboStream::Libs.partial_path(controller_path, template)
        disable_default = true
      else
        _template = [controller_path, action_name].join('/')
        disable_default = true
      end

      if disable_default && !@render_cable_template_was_called
        # make sure default render action returns nil
        render template: 'render_turbo_stream_empty_template', layout: false unless @render_cable_template_was_called
        @render_cable_template_was_called = true
      end

      # evaluate_instance_variables

      libs = RenderTurboStream::ChannelLibs.new
      loc = fetch_instance_variables(locals, _partial, _template, action)
      res = libs.render_to_channel(
        channel,
        RenderTurboStream::Libs.target_id_to_target(target_id),
        action,
        template: _template,
        partial: _partial,
        locals: loc
      )
      RenderTurboStream::Test::Request::Libs.set_test_responses(response, libs.test_responses)

      res
    end

    def action_to_all(action, *arguments)
      action_to_channel('all', action, arguments)
    end

    def action_to_me(action, *arguments)

      begin
        u_id = helpers.current_user&.id
        unless u_id.present?
          Rails.logger.debug('  • SKIP RenderTurboStream.action_to_me because current_user is nil')
          return
        end
      rescue
        Rails.logger.debug('  • ERROR RenderTurboStream.action_to_me because current_user is not available')
        return
      end

      action_to_channel("authenticated_user_#{helpers.current_user.id}", action, arguments)
    end

    def action_to_authenticated_group(group, action, *arguments)

      begin
        u_id = helpers.current_user&.id
        unless u_id.present?
          Rails.logger.debug('  • SKIP RenderTurboStream.action_to_authenticated_group because current_user is nil')
          return
        end
      rescue
        Rails.logger.debug('  • ERROR RenderTurboStream.action_to_authenticated_group because current_user method is not available')
        return
      end

      action_to_channel("authenticated_group_#{group}", action, arguments)
    end

    def action_to_channel(channel, command, arguments)
      libs = RenderTurboStream::ChannelLibs.new
      r = libs.action_to_channel(channel, command, arguments)
      RenderTurboStream::Test::Request::Libs.set_test_responses(response, libs.test_responses)
      r
    end

    def turbo_channel_save(
      save_action,
      object: nil, # object used in save_action, example: @customer

      if_success_redirect_to: nil, # does a regular redirect. Works if you are inside a turbo_frame and just want to redirect inside that frame BUT CANNOT STREAM OTHERS ACTIONS ON THE SAME RESPONSE https://github.com/rails/rails/issues/48056

      if_success_add: nil, # hash for a partial to render or array with actions (as array) or hashes for partials within
      if_error_add: nil, # additional partials that should be rendered if save_action failed
      add: [], # additional streams

      if_success_notices: nil, # array of strings, override default generated flash generation in the case of success
      if_error_alerts: nil,
      add_notices: nil, # array of strings
      add_alerts: nil,

      flash_controller_action_name: action_name # options: 'update', 'create', otherwise you have to declare a translation in config/locales like "activerecord.success.#{flash_controller_action_name}" and "activerecord.errors.#{flash_controller_action_name}"
    )

      # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      #                    LOGIC
      # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      unless object
        object = eval("@#{controller_name.classify.underscore}")
      end
      unless object
        raise "Could not fetch a model name by «eval(\"\@\#{controller_name.classify.underscore}\")». You must provide the argument :object."
      end

      libs = RenderTurboStream::ControllerLibs.new(save_action)
      model_name = object.model_name.human

      turbo_actions = libs.generate_flash(
        model_name,
        flash_controller_action_name,
        if_success_notices,
        if_error_alerts,
        add_notices,
        add_alerts,
      )[:turbo_actions]

      turbo_actions += libs.additional_actions(
        if_success_add,
        if_error_add,
        add
      )

      if libs.action_errors(turbo_actions).present?
        raise libs.action_errors(turbo_actions).join(', ')
      end

      if save_action
        if Rails.configuration.x.store_last_saved_object && object.id
          begin
            session['last_saved_object'] = object.to_global_id
          rescue
            Rails.logger.debug("session['last_saved_object'] = ... failed. You may be in test environent?")
          end
        end
      else
        RenderTurboStream::Libs.debug_save_errors(object, flash_controller_action_name)
      end

      # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      #==                      RENDER TO CHANNEL
      # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      libs = RenderTurboStream::ChannelLibs.new
      if (helpers.current_user.id rescue false)
        libs.send_actions_to_channel(
          "authenticated_user_#{helpers.current_user.id}",
          turbo_actions
        )
        RenderTurboStream::Test::Request::Libs.set_test_responses(response, libs.test_responses)
      end

      if save_action && if_success_redirect_to.present?
        response.status = 303
        redirect_to if_success_redirect_to
      end

    end

    private

    def fetch_instance_variables(locals, partial, template, action)
      _locals = (locals ? locals : {})

      # instance variables

      check_template = RenderTurboStream::CheckTemplate.new(
        partial: partial,
        template: template,
        available_instance_variables: self.instance_variables,
        action: action
      )

      # add instance_variables to locals

      check_template.templates_instance_variables.each do |v|
        _locals[:"render_turbo_stream_instance_variable_#{v}"] = self.instance_variable_get(v.to_s)
      end

      _locals
    end


  end
end