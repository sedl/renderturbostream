module RenderTurboStream
  class Channel

    def initialize(channel, partial: nil, template: nil, target_id: nil, action: :replace)
      @channel = channel
      @partial = partial
      @template = template
      @target_id = target_id
      @action = action
      @test_responses = []
    end

    def target_id=(target_id)
      @target_id = target_id
    end

    def action=(action)
      @action = action
    end

    def send(locals: {})
      libs = RenderTurboStream::ChannelLibs.new
      libs.render_to_channel(
        @channel,
        RenderTurboStream::Libs.target_id_to_target(@target_id),
        @action,
        template: @template,
        partial: @partial,
        locals: locals
      )
      @test_responses = libs.test_responses
    end

    def test_responses
      @test_responses
    end

    private

    def validate_params
      raise 'Arguments partial and template cannot both be specified' if @partial && @template
    end

  end
end