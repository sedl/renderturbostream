module RenderTurboStream
  class Libs

    def self.target_id_to_target(target_id)
      if target_id.present?
        if target_id[0] == '#'
          target_id
        else
          "##{target_id}"
        end
      else
        nil
      end
    end

    def self.target_to_target_id(target)
      if target.present? && target.to_s[0] == '#'
        target[1..-1]
      else
        nil
      end
    end

    def self.partial_path(controller_path, partial)
      if partial && partial.to_s.include?('/')
        partial
      elsif partial.present?
        [controller_path, partial].join('/')
      end
    end

    def self.template_path(controller_path, template)
      if template && template.to_s.include?('/')
        template
      elsif template.present?
        [controller_path, template].join('/')
      end
    end

    def self.fetch_arguments_from_rendered_string(rendered_string)
      noko = Nokogiri::HTML(rendered_string)
      frame = noko.at_css('turbo-frame')
      ele = noko.at_css('turbo-target')
      if frame.present?
        {
          target_id: frame[:id],
          target: target_id_to_target(frame[:id])
        }
      elsif ele.present?
        {
          target_id: ele[:id],
          target: target_id_to_target(ele[:id])
        }
      else
        {}
      end
    end

    def self.target_id(virt_view_path, id)

      if id.is_a?(String)
        id
      elsif id
        (
          (id.is_a?(Array) ? id : [id]).map do |_id|

            if _id.is_a?(String)
              _id
            elsif _id.methods.include?(:id)
              (
                _id.id ?
                  [_id.class.to_s.downcase, _id.id] :
                  ['new', _id.class.to_s.downcase]
              ).join('-')
            else
              _id.to_s
            end
          end +
            [
              virt_view_path.split('/').last.sub(/^_/, '')
            ]
        ).join('-')

      else
        [
          virt_view_path.gsub('/', '-').gsub('-_', '-')
        ].join('-')
      end
    end

    def self.debug_save_errors(object, controller_action_name)
      if object.id.present?
        Rails.logger.debug("  • #{controller_action_name} #{object.class}:#{object.id} FAILED:")
      else
        Rails.logger.debug("  • #{controller_action_name} #{object.class} FAILED:")
      end
      object.errors.full_messages.each do |e|
        Rails.logger.debug("    => #{e}")
      end
    end

  end
end