module RenderTurboStream
  class ControllerLibs

    def initialize(save_action)
      @save_action = save_action
    end

    def generate_flash(model_name, controller_action, if_success_notices, if_error_alerts, add_notices, add_alerts)

      target_id = (Rails.configuration.x.render_turbo_stream.flash_target_id rescue nil)
      raise "Missing configuration: config.x.render_turbo_stream.flash_target_id" unless target_id
      partial = Rails.configuration.x.render_turbo_stream.flash_partial
      raise "Missing configuration: configuration.x.render_turbo_stream.flash_partial" unless partial
      turbo_action = Rails.configuration.x.render_turbo_stream.flash_turbo_action
      raise "Missing configuration: configuration.x.render_turbo_stream.flash_turbo_action" unless turbo_action

      if @save_action
        notices = if if_success_notices
                    if if_success_notices.is_a?(String)
                      [if_success_notices]
                    else
                      if_success_notices
                    end
                  else
                    str = I18n.t(
                      "activerecord.render_turbo_stream_success.#{controller_action}"
                    )
                    [format(str, model_name: model_name)]
                  end
        alerts = []
      else
        alerts = if if_error_alerts
                   if if_error_alerts.is_a?(String)
                     [if_error_alerts]
                   else
                     if_error_alerts
                   end
                 else
                   str = I18n.t(
                     "activerecord.render_turbo_stream_errors.#{controller_action}"
                   )
                   [format(str, model_name: model_name)]
                 end
        notices = []
      end

      turbo_actions = []
      notices += add_notices.to_a
      alerts += add_alerts.to_a
      notices.each do |notice|
        next unless notice.present?
        flash_stream = {
          target: RenderTurboStream::Libs.target_id_to_target(target_id),
          partial: partial,
          action: turbo_action,
          locals: { success: true, message: notice }
        }
        turbo_actions.push(flash_stream)
      end
      alerts.each do |alert|
        next unless alert.present?
        flash_stream = {
          target: RenderTurboStream::Libs.target_id_to_target(target_id),
          partial: partial,
          action: turbo_action,
          locals: { success: false, message: alert }
        }
        turbo_actions.push(flash_stream)
      end
      { turbo_actions: turbo_actions, alerts: alerts, notices: notices }
    end

    def additional_actions(if_success_add, if_error_add, add)

      (@save_action ? make_actions(if_success_add) : make_actions(if_error_add)) + make_actions(add)

    end

    def generate_action(controller_path, target_id, action, partial, template, locals)
      libs = RenderTurboStream::Libs
      target = libs.target_id_to_target(target_id)
      if partial
      _partial = libs.partial_path(controller_path, partial)
      [target: target, partial: _partial, locals: locals, action: action]
      else
        _template = libs.template_path(controller_path, template)
        [target: target, template: _template, locals: locals, action: action]
      end
    end

    def action_errors(actions)
      r = []
      actions.each do |a|
        if a.is_a?(Hash)
          unless a[:target].present?
            r.push("Missing attribute :target for => «#{a}»")
          end
          unless a[:partial].present?
            r.push("Missing attribute :partial for => «#{a}»")
          end
          unless a[:action].present?
            r.push("Missing attribute :action for => «#{a}»")
          end
        elsif a.is_a?(Array)
          unless a.first.is_a?(String) || a.first.is_a?(Symbol)
            r.push("Assuming a command, but the first attribute must be a string or symbol => «#{a}»")
          end
        end
      end
      r
    end

    private

    # streamline actions to array
    # valid inputs for if_success_add or if_error_add or add
    # [:add_css_class, ...]
    # [[:add_css_class, ..], ..]
    # {target_id: '..', ..}
    # [{target_id: '..', ..}, ..]

    def make_actions(actions)
      res = if actions.present?
              if actions.is_a?(Hash)
                [actions]
              elsif actions.is_a?(Array)
                if actions.first.is_a?(String) || actions.first.is_a?(Symbol)
                  [actions]
                else
                  actions
                end
              end
            else
              []
            end
      res.map do |a|
        if a.is_a?(Hash) && a.key?(:target_id)
          a = a.merge(target: "##{a[:target_id]}")
          a.delete(:target_id)
          unless a.key?(:action)
            a[:action] = :replace
          end
          a
        else
          a
        end
      end
    end

  end
end