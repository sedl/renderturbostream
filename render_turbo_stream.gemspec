require_relative "lib/render_turbo_stream/version"

Gem::Specification.new do |spec|
  spec.name = "render_turbo_stream"
  spec.version = RenderTurboStream::VERSION
  spec.authors = ["christian"]
  spec.email = ["christian@sedlmair.ch"]
  spec.homepage = "https://gitlab.com/sedl/renderturbostream"

  spec.summary = "rails-like workflow for turbo streams and channels. With testing strategy."

  spec.summary = "Make working with Turbo fun!"
  spec.description = "A set of helpers that allow a UNIFIED WORKFLOW for TurboStream and TurboStreams::Channel. To avoid a heavy mix of view and logic, partials and templates can be rendered directly from the controller. There is no need to write *.turbo_stream.* templates anymore. Logic can stay on in the controller. Javascript actions can also be executed directly from the controller. TESTING: To reduce the amount of hard-to-maintain system tests, request tests are possible. A DEMO PROJECT with all this built in, along with system and request tests, is linked in the README."

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/sedl/renderturbostream"
  spec.metadata["changelog_uri"] = "https://gitlab.com/sedl/renderturbostream"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.3"
  spec.license = "MIT"
end
