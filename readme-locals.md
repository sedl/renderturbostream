Back to [render_turbo_stream](https://gitlab.com/sedl/renderturbostream)

# Rendering Partials with locals

If you work with `render_turbo_stream` everything should work as expected. But I had to do some configs to make this possible and it is good to know the pitfalls, see [rails-issue](https://github.com/rails/rails/issues/48051)

## Usual / Gem

Unlike [docs](https://guides.rubyonrails.org/layouts_and_rendering.html#passing-local-variables), in general, you render a partial from within a template by doing something like this:

```haml
= render 'person', name: 'hugo'
```

Then the variable is available inside the partial:

```
<%= name %>
```

When using this gem, locals are passed with the `:locals' argument, and as of version 4.3, they are available inside the partial in the same way.

## Different cases

As long as the above problem is not solved by Rails and you have problems:

A workaround, to make sure a variable works for every case, you could put some code like this inside the partial:

```haml
- message = ((message rescue false) || (locals[:message] rescue false) || local_assigns[:message])
```
