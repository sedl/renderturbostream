This is part of [render_turbo_stream README](https://gitlab.com/sedl/renderturbostream)

# ActionCable integration

[turbo_rails](https://github.com/hotwired/turbo-rails) integrates `ActionCable` as `Turbo::StreamsChannel`.

Turbo Stream relies on the Render function, which can only be performed once per request and conflicts with a Redirect,
and can only be a response to a request.

Channels are independent.

So called "authenticated" channels are based on the `devise` gem. If you are not on devise, you will need to provide
a `current_user` method for `ApplicationHelper`.

A quick and dirty application with all the features, including tests, built in
is [here](https://gitlab.com/sedl/renderturbostream_railsapp).

**The way of using channels for actions described here is very new!** I am curious to hear your experiences.

# Installation

After [render_turbo_stream](https://gitlab.com/sedl/renderturbostream) please follow all installation steps
from [turbo-rails#streams](https://github.com/hotwired/turbo-rails#come-alive-with-turbo-streams):

create a **initializer** for including the view helpers:

```ruby
ActionView::Base.send :include, RenderTurboStream::ChannelViewHelpers
```

**ApplicationController**

```ruby
include RenderTurboStream::ChannelControllerHelpers
```

If you are on Vite, make sure you have this line in **application.js**

```javascript
import '@hotwired/turbo-rails'
```

**application-layout.haml**

Select which one you want to enable:

```haml
= channel_from_all
= channel_from_me
= channel_from_authenticated_group(current_user&.group)
```

When a user is logged in that establishes a cable connection, such as `all`, `authenticated_user_2`
and `authenticated_group_staff`.

The `channel_from_all` method would establish a channel to ALL VISITORS, included those which are not autenticated.

When you reload your view with an authenticated user, you should see something like this in the logs:

```
18:48:47 web.1  | Started GET "/cable" for 127.0.0.1 at 2023-04-29 18:48:47 +0200
18:48:47 web.1  | Started GET "/cable" [WebSocket] for 127.0.0.1 at 2023-04-29 18:48:47 +0200
18:48:47 web.1  | Successfully upgraded to WebSocket (REQUEST_METHOD: GET, HTTP_CONNECTION: keep-alive, Upgrade, HTTP_UPGRADE: websocket)
18:48:47 web.1  | Turbo::StreamsChannel is transmitting the subscription confirmation
18:48:47 web.1  | Turbo::StreamsChannel is streaming from authenticated_user_2
```

# Usage

## Controller Helper

From the controller, you can now execute commands such as:

```ruby
    render_to_me(
  'message-box', #=> target html-id
  locals: {
    comment: "hello from turbo channel"
  }
)
```

This can be called multiple times for a request. Providing a `partial:` attribute would render a partial.

All methods with `*_to_me` target the currently logged in user. In the same way, `render_to_all`
or `render_to_authenticated_group` work. For the latter, the first argument is the group name, but this method only
works when a user is logged in.

Locals are passed through a `.symbolize_keys`! So inside the partial you always have to fetch the locals hash by symbol
key.

With the `turbo_power` gem such things are possible:

```ruby
action_to_me(
  'add_css_class',
  '#colored-element',
  'background-red'
)
```

**Instance variables**

Unlike rendering partials or templates in the classic way, instance variables do not work on channels. This gem includes
a helper that is able to fetch them and sets them inside the partial:

```haml
- fetch_instance_variables(((locals rescue false) || (local_assigns rescue nil)))
%p
  = "@var has the content: #{@var}"
```

How can this happen? As described in the main README, on the first call after a restart, or if precompile assets are
defined, it regexes through a partial and its children. Children can only be mapped if the partial name is hardcoded.
Partials declared by a variable cannot be traced.

When the partial is called, it fetches the value from the controller and adds it to locals using a namespaced key. From
there, the aforementioned helper can fetch and place it. If the same part is rendered in a way other than by these
channel helpers, the helper does nothing.

**Template**

```ruby

def custom_action
  render_to_me(
    'render-to-box',
    locals: {
      comment: "rendered default template to me (#{helpers.current_user.name})"
    }
  )
end
```

If **no partial attributes are defined**, it renders the template derived from the controller action name to the channel
for the authenticated user.

**Actions**

As described for `render_to_me`, `action_to_*` is possible. Performing an action from the `turbo_power` gem on the
currently logged in user would look like this:

```ruby
   action_to_me(
  'add_css_class',
  '#colored-element',
  'background-red'
)
```

## Class Methods

Turbo Channel is controller independent, which means it can be called from anywhere. In return, it has less handy helpers:

- Cannot evaluate relative paths to partials or templates which means you always have to give the full relative path,
  for example `articles/form` instead of just `form`
- In the case of Test Environment: Does not automatically include a copy of the rendered output in the response to provide the request to test helpers.
- It cannot fetch the `instance variables` from the controller and render them to the partials (Useful for Turbo Channels only)


**Example**

Within a controller:

```ruby

def info
  channel = RenderTurboStream::Channel.new(
    :channel_to_all, #=> the channel name
    partial: 'articles/info'
  )
  channel.send(locals: {..})

  # for testing: add the responses to the response header to provide response testing
  RenderTurboStream::Test::Request::Libs.set_test_responses(response, channel.test_responses)
end
```

# Testing

Same as described for Turbo Stream actions: The idea is to do most of the testing with request tests. Some system
testing, e.g. by Capybara, is necessary to make sure that all this stuff works together, but most testing can be done by
the much faster request tests.

Request tests cannot test javascript! They only test the ruby side: Verify that the appropriate content is being sent to
turbo.

Example for rspec:

```ruby
  it 'make red to all' do
  post channel_make_red_path
  expect(turbo_targets.length).to eq(1)
  expect(all_turbo_responses.length).to eq(2)
  assert_channel_to_all('colored-element', action: 'add_css_class')
  assert_channel_to_all('colored-element', action: 'remove_css_class') { |args| args.last == 'background-green' }
end
```

Check if a flash message is sent to the current user:

```ruby
assert_channel_to_me(user, 'flash-box') { |p| p.css('.callout.success').inner_html.to_s.include?('Article successfully created') }
```

# Contributors welcome

Back to [render_turbo_stream README](https://gitlab.com/sedl/renderturbostream).
